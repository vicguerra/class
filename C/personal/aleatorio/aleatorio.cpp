#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<time.h>

int main (int argc, char *argv[]) {
	srand (time (NULL));
	int i, numProp, numAleat = (rand () % 100) + 1;
	// printf ("%i", numAleat);
	printf ("Voy a pensar un número del 0 al 100\nTienes 5 intentos.\nSuerte\n");
	for (i = 1; i <= 5; i++) {
		printf ("Cual crees que es?: ");
		scanf (" %d", &numProp);
		printf ("\n");
		if (numProp == numAleat)
			break;
		else if (numProp > numAleat)
			printf ("Error, el numero que buscas es menor.\n");
		else
			printf ("Error, el numero que buscas es mayor.\n");
	}
	if (numProp == numAleat)
		printf ("Has ganado!\nEl numero era el %i. Enhorabuena!\nIntentos: %i", numAleat, i);
	else
		printf ("Has perdido...\nEl numero era el %i. Otra vez sera...", numAleat);
	printf ("\n");
	return EXIT_SUCCESS;
}
