USE PRACTICA_DIEGO;

ALTER TABLE COMPANIA ADD COLUMN `CEO` VARCHAR(30) NOT NULL DEFAULT 'Diego Martin' AFTER logo;

-- La pongo como unique
ALTER TABLE COMPANIA MODIFY `CEO` VARCHAR(30) NOT NULL DEFAULT 'Diego Martin' UNIQUE;

DESC COMPANIA;

-- Le quito el unique
ALTER TABLE COMPANIA MODIFY `CEO` VARCHAR(30) NOT NULL DEFAULT 'Diego Martin';

-- Demasiado ego asi que la quito
ALTER TABLE COMPANIA DROP COLUMN `CEO`;

-- Compruebo que existe una primary key en `VUELO`
DESC VUELO;

-- Borro la primary key `VUELO`.`estado` que habia creado a proposito para borrarla
ALTER TABLE VUELO DROP PRIMARY KEY, ADD PRIMARY KEY (codigoVuelo);

-- Compruebo que está borrada
DESC VUELO;