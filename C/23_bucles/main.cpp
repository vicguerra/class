#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  /* Declaracion de variables */
  unsigned num;
  bool c = true;

  /* Entrada de datos */
  printf ("Escribe un número: ");
  scanf (" %u", &num);
   
  /* Encuentra los divisores del número */
  for (int i = 1; i < num; i++) {
    if ((num % i) == 0) {
      printf ("%i.\n", i);
      if (i != num && i != 1)
        c = false;
    }
  }
  printf ("%i.\n", num);
  if (c)
    printf ("El número %i es primo.\n", num);
  return EXIT_SUCCESS;
}
