#define DIM 3

enum TBase {cart, cil, esf, BASES};

const char *const bases[] = {
  "cartesianas",
  "cilindricas",
  "esfericas",
  NULL
};

const char * const componentes [][DIM] = {
  {"X", "Y", "Z"},
  {"Ro", "Theta", "Z"},
  {"R", "Theta", "phi"}
};