#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  int *p; // Declaras una variable con un adirección de memoria

  p = (int *) malloc (3 * sizeof (int)); // Le asignas una capacidad maxima
  
  *p = 2; // Metes un número en esa dirección
  *(p + 1) = 7;
  p[2] = 9;

  printf ("%i - %i - %i\n", *p, p[1], *(p + 2));

  p = (int *) realloc (p, 7 * sizeof (int)); // Amplia lo que hay en 'p' y lo fija en este caso a (7 * sizeof(int)) == (7 * 4) == (28 bytes) //

  free (p); // Para limpiar p, y recuperar memoria
  return EXIT_SUCCESS;
}
