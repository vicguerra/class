#include<stdio.h>
#include<unistd.h>
#define MAXCOLS 100

int main () {
	/*
	 * \x0A = \n
	 * \x09 = \t
	 * \x07 = \a
	 */
	printf ("Hola (Hex) \x0A \x09 Beep \x07 \x0A");
	printf ("Hola (Oct) \012 \011 Beep \007 \012");
	printf ("\0x0A Esto es secreto"); // Despues de '\0' termina el string
	printf ("\n");
	for (int vez = 0; vez<=MAXCOLS; vez++) {
		printf ("\r");
		for (int igual = 0; igual<vez; igual++)
			printf ("=");
		printf ("> %2i%%", vez);
		fflush (stdout);
		usleep (100000);
	}
	printf ("\nGG");
	printf ("\n");
	return 0;
}
