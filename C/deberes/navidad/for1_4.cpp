#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main (int argc, char *argv[]) {
  int ene;
  char nombre[40];
  printf ("Que nombre imprimo?: ");
  fgets(nombre, 40, stdin);
  nombre[strcspn(nombre, "\r\n")] = 0; // Esta línea quita el 'intro' que se cuela al aceptar el fgets
  printf ("Cuantas veces lo imprimo?: ");
  scanf (" %d", &ene);
  for (int i = 0; i < ene; i++) {
    printf ("%s\n", nombre);
  }
  return EXIT_SUCCESS;
}
