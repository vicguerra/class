#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  /* Declaracion de variables */
  unsigned num;
  bool c = true;

  /* Entrada de datos */
  printf ("Escribe un número: ");
  scanf (" %u", &num);
   
  /* Encuentra los divisores de todos los números hasta el número */
  for (int i = 1; i <= num; i++) {
    printf ("%i.-------------\n", i);
    for (int j = 1; j <= i; j++) {
      if ((i % j) == 0)
        printf ("\t%i.\n", j);
    }
  }
 // printf ("%i.\n", num);
  return EXIT_SUCCESS;
}
