#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main (int argc, char *argv[]) {
	char color = 'r';
	printf ("Introduce la inicial de un color (r, g, b): ");
	scanf (" %c", &color);
	
	switch (tolower(color)) {
		case 'r':
		case 'g':
		case 'b':
			printf ("El caracter '%c' es valido\n", color);
			break;
		default:
			printf ("Ese no es ningun color de los esperados :)\n");
	}
	return EXIT_SUCCESS;
}

