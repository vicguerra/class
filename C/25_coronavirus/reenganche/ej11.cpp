#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
	/* Datos */
	double temperatura = 0;
	
	/* Entrada de datos */
	printf ("Introduce la temperatura.\nTe dire en que estado estael agua\nCual es la temperatura?: ");
	scanf (" %lf", &temperatura);
	
	/* Salida */
	printf ("El agua esta en estado ");
	if (temperatura < 1)
		printf ("Solido\n");
	else if (temperatura > 0 && temperatura < 100)
		printf ("Liquida\n");
	else
		printf ("Gas\n");
	return EXIT_SUCCESS;
}
