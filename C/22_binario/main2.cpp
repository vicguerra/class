#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  /* Definicion de variables */
  int num, i = 1, res = 0;

  /* Entrada de datos */
  printf ("Dime un número y lo paso a binario: ");
  scanf (" %i", &num);

  /* Tratamiento de datos */
  for (; num > 0; i *= 10, num >>= 1) {
    res += (num % 2) * i;
  }
  printf ("%i\n", res);
  return EXIT_SUCCESS;
}
