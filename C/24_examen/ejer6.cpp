#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  int cont = 0, num = 1, sum = 0;

  do {
    sum = 0;
    for (int i = 0; i < num; i++)
      if (num % i == 0)
        sum += i;

    if (sum == num) {
      cont++;
      printf ("%i\n", num);
    }
    num++;
  } while (cont < 3);

  return EXIT_SUCCESS;
}
