#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  char *frase = NULL;
  char letra;
  int charleidos = 0;

  printf ("Nombre: ");
  while ((letra = (char) getchar ()) != '\n') {
    frase = (char *) realloc (frase, ++charleidos * sizeof (char));
    *(frase + charleidos - 1) = letra;
  }
  frase = (char *) realloc (frase, ++charleidos * sizeof (char));
  *(frase + charleidos - 1) = '\0';
  
  printf ("Hola %s!\n", frase);

  free (frase);
  return EXIT_SUCCESS;
}
