#include <stdio.h>

#define N 20

int main () {
  int num[N];
  int acumulado = 0;
    
  for (int i=0; i<N; i++){
    num[i] = i * i;
    acumulado += num[i];
  }
        /* Pon en cada celda los cuadrados de los números naturales 
        (que empiezan en 1) */
        
    /* Usando un bucle y el operador  taquigráfico += guarda en acumulado
    la suma de todas las celdas */
    
    /* Imprime el resultado */
  printf ("Acumulado: %i\n", acumulado);
  for (int i = 0; i < N; i++) {
    printf ("Cuadrado de %i: %i\n", i, num[i]);
  }
  return 0;
}

