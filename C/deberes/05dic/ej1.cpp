#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
    for (int carta = 0xA1829FF0, veces = 0; veces < 12; carta += 0x01000000, veces++)
        printf ("%s (%X)\n", (char *) &carta, carta);
    printf ("\n");
    return EXIT_SUCCESS;
}
