#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
    int a = 4, b = 0, c = a, d = 0;
    printf ("a = %i\nb = %i\nc = %i\nd = %i\n", a, b, c, d);
    printf ("-------------------\n");
    b = ++a;
    d = c++;
    printf ("b = ++a\nd = c++\n");
    printf ("-------------------\n");
    printf ("a = %i\nb = %i\nc = %i\nd = %i", a, b, c, d);
    printf ("\n");
    return EXIT_SUCCESS;
}
