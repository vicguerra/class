#include<stdio.h>
#include<stdlib.h>
#define MAX 0x100

int atoi2 (char number[MAX]) {
  int resultado = 0;
  for (int i = 0; number[i] != '\0'; i++)
    resultado = resultado * 10 + number[i] - '0';
  return resultado;
}

int main (int argc, char *argv[]) {
  char leido[MAX];
  int numero;
  printf ("Entero: ");
  scanf (" %[0-9]", leido);
  //---------------------------------------------------------------------------------------//
  // "atoi()" (ascii to integer) pasa una cadena de caracteres formada por números a 'int' //
  // --------------------------------------------------------------------------------------//
  numero = atoi2 (leido);
  printf ("Numero: %i\n", numero);
  return EXIT_SUCCESS;
}
