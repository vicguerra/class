#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  float triangulo[3] = {3.0, 4.0, 5.0};
  float escala = 1.5;
  printf ("Triangulo inicial:\ncateto1: %2.2f.\ncateto2: %2.2f.\nhipotenusa: %2.2f.\n----------\nTriangulo final:\ncateto1: %2.2f.\ncateto2: %2.2f.\nhipotenusa: %2.2f.\n", triangulo[0], triangulo[1], triangulo[2], triangulo[0] * escala, triangulo[1] * escala, triangulo[2] * escala);
  return EXIT_SUCCESS;
}
