#include <stdio.h>

#define N 6

int main () {
    char nombre[N] = { 'V', 'i', 'c', 't', 'o', 'r' };
    /* Al no estar presente el valor centinela '\0' tenemos que recorrer todas
    las celdas.  */
    for (int i=0; i<N; i++)
       printf ("%c", nombre[i]);
    
		printf ("\n");
    return 0;
}
