#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  int d, m, y, numitems;
  printf ("dd/mm/yyyy: ");
  // numitems recoje el número de inputs por scanf
  // si entre el % y la 'i' del '%i' escribes un '*' puedes leer el input pero no guardarlo
  numitems = scanf (" %i/%i/%i", &d, &m, &y);
  printf ("Has puesto %i/%i/%i.\n", d, m, y);
  printf ("Total: %i valores.\n", numitems);
  return EXIT_SUCCESS;
}
