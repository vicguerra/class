#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  double buffer;
  double *vec = NULL;
  static int dim = 0;
  char end;
  int componente;

  printf ("Ejemplo: (8.5 5 6.3).\tVector: ");
  scanf (" %*[(]");
  do {
    vec = (double *) realloc (vec, (dim + 1) * sizeof(double));
    scanf (" %lf", &buffer);
    vec[dim++] = buffer;
  } while (!scanf (" %1[)]", &end));

  printf ("\n(");
  for (componente = 0; componente < (dim - 1); componente++)
    printf ("%.2lf, ", vec[componente]);
  printf ("%.2lf)\n", vec[componente]);

  free (vec);
  return EXIT_SUCCESS;
}
