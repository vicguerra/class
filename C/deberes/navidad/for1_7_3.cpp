#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  printf ("Figura 3:\n");
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 8; j++) {
      if (i == 0 || i == 4 || j == 0 || j == 7)
        printf ("*");
      else
        printf (" ");
    }
    printf ("\n");
  }
  return EXIT_SUCCESS;
}
