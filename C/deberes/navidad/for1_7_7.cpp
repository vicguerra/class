#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  printf ("Figura 7:\n");
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      if (i == 0 || i == 7 || j == 0 || j == 7 || i == j || (7 - i) == j)
        printf ("*");
      else
        printf (" ");
    }
    printf ("\n");
  }
  return EXIT_SUCCESS;
}
