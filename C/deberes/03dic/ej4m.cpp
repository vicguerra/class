/*
#################################
##       Diego Martin          ##
#################################
##     Baraja preguntando      ##
#################################
*/
#include<stdio.h>
#include<stdlib.h>

bool quieres (char palo[8]) {
    char respuesta;
    printf ("Imprimo %s? (s/n): ", palo);
    scanf (" %s", &respuesta);
    if (respuesta == 's') {
        return true;
    } else if (respuesta == 'n') {
        return false;
    } else {
        return false;
    }
    return true;
}

int main (int argc, char *argv[]) {
    char palos[5][8] = {"Oros", "Espadas", "Bastos", "Copas"};
    char num[10][8] = {"As", "2", "3", "4", "5", "6", "7", "Sota", "Caballo", "Rey"};
    char simbolos[5][8] = {"🥇", "⚔️", "🥢", "🍷"};
    bool palosSelected[5];
    for (int i = 0; i < 4; i++) {
        palosSelected[i] = quieres (palos[i]);
    }
    printf ("----------------------\n");
    for (int p = 0; p < 4; p++) {
      if (palosSelected[p]) {
        for (int i = 0; i < 10; i++) {
           printf ("%s de %s (%s)\n", num[i], simbolos[p], palos[p]);
        }
        printf ("----------------------\n");
      }
    }
    return EXIT_SUCCESS;
}
