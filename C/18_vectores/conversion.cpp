#include<stdio.h>
#include<stdlib.h>

#define DIM 3

const char *const bases[] = {
  "cartesianas",
  "cilindricas",
  "esfericas",
  NULL
};

const char * const componentes [][DIM] = {
  {"X", "Y", "Z"},
  {"Ro", "Theta", "Z"},
  {"R", "Theta", "phi"}
};
enum TBase {cart, cil, esf, BASES};

void titulo () {
  system ("clear");
  system ("toilet -fpagga --gay Coordenadas");
  printf ("\n\n");
}

void printOptions (const char *const estadio, enum TBase elegida = BASES) {
  const char **base = (const char **) bases;

  printf ("Elige la base %s:\n", estadio);

  for (int i = 0; *base != NULL; base++, i++)
    if (elegida != i)
      printf ("\t%i.- %s\n", i + 1, *base);

  printf ("\n");
  //return elegida;
}

enum TBase preguntaOpt (enum TBase exception = BASES) { 
  unsigned eleccion;
  const char **opts = (const char **) bases;
  do {
    printf ("                                                   \r");
    printf ("Opcion: ");
    scanf (" %u", &eleccion);
    printf ("\x1B[1A");
    fflush (stdout);
  } while ((eleccion < 0 && eleccion > BASES) && (exception == eleccion));
  eleccion--;
  printf ("Elegiste: %s.\n", opts[eleccion]);
  return (enum TBase) eleccion;
}

void menu (enum TBase *src, enum TBase *dst) {
  titulo ();
  printOptions ("inicial", (enum TBase) BASES);
  *src = preguntaOpt ();
  titulo ();
//  printf ("%i\n", (enum TBase) *src);
  printOptions ("destino", (enum TBase) *src);
  *dst = preguntaOpt ((enum TBase) *src);
}

void preguntaVector (enum TBase base, double vector[DIM]) {
  titulo ();
  printf ("Componentes: \n\n");
  for (int c = 0; c < DIM; c++) {
    printf ("%s: ", componentes[base][c]);
    scanf (" %lf", &vector[c]);
  }
}

int main (int argc, char *argv[]) {
  enum TBase srcbas, dstbas;
  double srv_vec[DIM], dst_vec[DIM];

  menu ( &srcbas, &dstbas );

  preguntaVector ( srcbas, srv_vec );

  // if (srcbas == dstbas); // No lo necesito

  printf ("\n");
  return EXIT_SUCCESS;
}