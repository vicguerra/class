#include <stdio.h>
#include <stdlib.h>
#include "interfaz.h"

void titulo () {
  system ("clear");
  system ("toilet -fpagga --gay Coordenadas");
  printf ("\n\n");
}

void printOptions (const char *const estadio, enum TBase elegida = BASES) {
  const char **base = (const char **) bases;

  printf ("Elige la base %s:\n", estadio);

  for (int i = 0; *base != NULL; base++, i++)
    if (elegida != i)
      printf ("\t%i.- %s\n", i + 1, *base);

  printf ("\n");
  //return elegida;
}

enum TBase preguntaOpt (enum TBase exception = BASES) { 
  unsigned eleccion;
  const char **opts = (const char **) bases;
  do {
    printf ("                                                   \r");
    printf ("Opcion: ");
    scanf (" %u", &eleccion);
    printf ("\x1B[1A");
    fflush (stdout);
  } while ((eleccion < 0 && eleccion > BASES) && exception == eleccion);
  eleccion--;
  printf ("Elegiste: %s.\n", opts[eleccion]);
  return (enum TBase) eleccion;
}

void menu (enum TBase *src, enum TBase *dst) {
  titulo ();
  printOptions ("inicial", (enum TBase) BASES);
  *src = preguntaOpt ();
  titulo ();
//  printf ("%i\n", (enum TBase) *src);
  printOptions ("destino", (enum TBase) *src);
  *dst = preguntaOpt ((enum TBase) *src);
}

void preguntaVector (enum TBase base, double vector[DIM]) {
    titulo ();
    printf ("Componentes: \n\n");
    for (int c = 0; c < DIM; c++) {
      printf ("%s: ", componentes[base][c]);
      scanf (" %lf", &vector[c]);
    }
}

/*void imprimeVector () {
  printf("\n\n");
  printf("Vector [%s]: %s", );
}*/