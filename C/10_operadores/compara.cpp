#include<stdio.h>
#include<stdlib.h>
#define MAX_ERROR .00001

int main (int argc, char * argv[]) {
    double user_number;
    printf ("Number?: ");
    scanf (" %lf", &user_number);
    if (user_number >= 3. - MAX_ERROR &&
        user_number <= 3. + MAX_ERROR)
        printf ("Numero: %f", user_number);
    else
        printf ("El número no es correcto jaja!");
    printf ("\n");
    return EXIT_SUCCESS;
}
