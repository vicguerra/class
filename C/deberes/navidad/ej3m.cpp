#include <stdio.h>
#include <stdlib.h>

#define PAPUP 8
#define MAMUP 4
#define HIJOP 2
#define HIJAP 1

enum TMiembro {
    papa,
    mama, 
    hijo,
    hija,
    /* Define aquí el resto de miembros de la familia. */
    total_miembros
};


/* conmutar: devuelve el valor old_status habiéndo cambiado el valor del bit correspondiente
al miembro de la familia */
int conmutar (int old_status, enum TMiembro miembro) {
  int bit;
	switch (miembro) {
        /* Realiza la operación a nivel de bits necesaria para
           cambiar el bit correspondiente. Usa los valores en los
           define. No uses if. */
        /* Rellena todos los casos */
    case papa:
      bit = PAPUP;
      break;
    case mama:
      bit = MAMUP;
      break;
    case hijo:
      bit = HIJOP;
      break;
    case hija:
      bit = HIJAP;
      break;
    default:
      bit = 0;
      break;
  }
  return old_status ^ bit;
}

/* acostar: devuelve el valor old_status habiéndo puesto a 0 el bit correspondiente
al miembro de la familia */
int acostar (int old_status, enum TMiembro miembro) {
    int bit = 1, pos = (total_miembros - 1) - miembro; /* Invierto los miembros */
    for (int vez = 0; vez > pos; vez++) /* bit = PAPUP | MAMUP | HIJOP | HIJAP */
        bit = bit << 1;
        /* Realiza la operación a nivel de bits necesaria para
           poner a 0 el bit correspondiente. Usa los valores en los
           define */
    return old_status & ~bit;
}

/* levantar: devuelve el valor old_status habiéndo levantado el bit correspondiente
al miembro de la familia */
int levantar (int old_status, enum TMiembro miembro) {
    int bit = 1, pos = (total_miembros - 1) - miembro;
    for (int vez = 0; vez < pos; vez++)
        bit = bit << 1;
        /* Realiza la operación a nivel de bits necesaria para
           poner a 1 el bit correspondiente. Usa los valores en los
           define */
    return old_status | bit;
}

/* Imprime con texto el estado de cada miembro de la familia*/
void print (int waked, enum TMiembro miembro) {
    switch (miembro) {
        case papa:
            printf ("Papá está %s\n", waked && PAPUP ? "levantado": "acostado");
        case mama:
            printf ("Papá está %s\n", waked && PAPUP ? "levantado": "acostado");
        case hijo:
            printf ("Papá está %s\n", waked && PAPUP ? "levantado": "acostado");
        case papa:
            printf ("Papá está %s\n", waked && PAPUP ? "levantado": "acostado");
    }
}

int main () {
    int waked = 0;
    enum TMiembro despertado = papa; // 
    print (waked); 
    waked = levantar (waked, papa);
    print (waked);
    waked = acostar (waked, papa);
    print (waked);
    waked = conmutar (waked, papa);
    
    print (waked);
    
    return EXIT_SUCCESS;
}
