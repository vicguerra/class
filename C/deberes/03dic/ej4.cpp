/*
#################################
##       Diego Martin          ##
#################################
##           Baraja            ##
#################################
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main (int argc, char *argv[]) {
  char palos[5][8] = {"Oros", "Espadas", "Bastos", "Copas"};
  char num[10][8] = {"As", "2", "3", "4", "5", "6", "7", "Sota", "Caballo", "Rey"};
  char simbolos[5][8] = {"🥇", "⚔️", "🥢", "🍷"};
  printf("----------------------\n");
  for (int p = 0; p < 4; p++) {
    for (int i = 0; i < 10; i++) {
      printf ("%s de %s (%s)\n", num[i], simbolos[p], palos[p]);
    }
    printf ("----------------------\n");
  }
  return EXIT_SUCCESS;
}
