/* ---------------------------------------------------------------------
 * Dados dos vectores tridimensionales decir que angulo hay entre ellos.
 * ---------------------------------------------------------------------
 */
#include <stdio.h>
#include <stdlib.h>

#define DIM 3

const char *const ejes[DIM] = {"X", "Y", "Z"};

void titulo () {
  system ("clear");
  system ("toilet -fpagga --gay Rest_Vec_3D");
  printf ("\n\n");
}

void preguntaVector (double vecIni[DIM], const char *const type = "inicial") {
  printf ("Valores del vector %s en medidas cartesianas: \n\n", type);
  for (int i = 0; i < DIM; i++) {
    printf ("\t%s: ", ejes[i]);
    scanf (" %lf", &vecIni[i]);
  }
}

void printProductoEscalar (double vecIni[DIM], double vecFin[DIM]) {
  printf ("\n\tEl producto escalar de (%.2lf, %.2lf, %.2lf) y (%.2lf, %.2lf, %.2lf) es:\n\n", vecIni[0], vecIni[1], vecIni[2], vecFin[0], vecFin[1], vecFin[2]);
  printf ("\t(");
  for (int i = 0; i < (DIM - 1); i++)
    printf ("%.2lf, ", vecIni[i] * vecFin[i]);
  printf ("%.2lf)\n", vecIni[(DIM - 1)] * vecFin[(DIM - 1)]);
}

int main (int argc, char *argv[]) {
  double vecIni[DIM], vecFin[DIM];
  titulo ();
  preguntaVector (vecIni);
  titulo ();
  preguntaVector (vecFin, "a restar");
  titulo ();
  printf ("Resultado del producto escalar: \n");
  printProductoEscalar (vecIni, vecFin);
  printf ("\n");
  return EXIT_SUCCESS;
}
