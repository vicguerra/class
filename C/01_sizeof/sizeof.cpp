#include <stdio.h>

int main(){
  printf("int: %u\n", sizeof(int));
  printf("short int: %u\n", sizeof(short int));
  printf("long int: %u\n", sizeof(long int));
  printf("long long int: %u\n", sizeof(long long int));
  printf("signed int: %u\n", sizeof(signed int));
  printf("unsigned int: %u\n", sizeof(unsigned int));
  printf("char: %u\n", sizeof(char));
  printf("char[20]: %u\n", sizeof(char[20]));
  printf("double: %u\n", sizeof(double));
  printf("long double: %u\n", sizeof(long double));
  return 0;
}
