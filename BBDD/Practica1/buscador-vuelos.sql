DROP DATABASE IF EXISTS PRACTICA_DIEGO;
CREATE DATABASE PRACTICA_DIEGO CHARACTER SET utf8 COLLATE utf8_bin;

USE PRACTICA_DIEGO;

CREATE TABLE IF NOT EXISTS AEROPUERTO (
	codigoIATA CHAR(3),
	nombre VARCHAR(50),
	ciudad VARCHAR(30),
	pais VARCHAR(35),
	CONSTRAINT codigoIATA_PK PRIMARY KEY (codigoIATA)
);

CREATE TABLE IF NOT EXISTS TERMINAL (
	numero INT(2),
	codigoIATA CHAR(3),
	CONSTRAINT codigoIATA_FK FOREIGN KEY (codigoIATA) REFERENCES AEROPUERTO(codigoIATA)
);

CREATE TABLE IF NOT EXISTS COMPANIA (
	codigoCompania CHAR(2),
	nombre VARCHAR(50),
	logo LONGBLOB NOT NULL,
	CONSTRAINT codigoCompania_PK PRIMARY KEY (codigoCompania)
);

CREATE TABLE IF NOT EXISTS VUELO (
	codigoVuelo VARCHAR(7),
	codigoCompania CHAR(2),
	aeropuertoOrigen CHAR(3),
	aeropuertoDestino CHAR(3),
	estado ENUM("En curso", "En espera", "Con demora", "En salidas", "En llegadas") NOT NULL DEFAULT "En espera",
	fecha DATETIME,
	CONSTRAINT VUELO_PK PRIMARY KEY (codigoVuelo, estado), -- Creada a proposito para quitar 'estado' en 'buscador-vuelos-mod.sql'
	CONSTRAINT codigoCompania_FK FOREIGN KEY (codigoCompania) REFERENCES COMPANIA(codigoCompania),
	CONSTRAINT aeropuertoOrigen_FK FOREIGN KEY (aeropuertoOrigen) REFERENCES AEROPUERTO(codigoIATA),
	CONSTRAINT aeropuertoDestino_FK FOREIGN KEY (aeropuertoDestino) REFERENCES AEROPUERTO(codigoIATA)
);

CREATE TABLE IF NOT EXISTS ASIENTO (
	codigoAsiento CHAR(3),
	codigoVuelo VARCHAR(7),
	tipoClase ENUM("Primera clase", "Segunda clase", "Turista", "Pobre"),
	CONSTRAINT codigoVuelo_PK FOREIGN KEY (codigoVuelo) REFERENCES VUELO(codigoVuelo)
);

CREATE TABLE IF NOT EXISTS PASAJERO (
	DNI VARCHAR(9),
	nombre VARCHAR(25),
	apellido1 VARCHAR(25),
	apellido2 VARCHAR(25),
	CONSTRAINT DNI_PK PRIMARY KEY (DNI)
);

CREATE TABLE IF NOT EXISTS RESERVA (
	localizador CHAR(6),
	DNIPasajero VARCHAR(9),
	precio FLOAT(7,2),
	fecha DATETIME,
	CONSTRAINT localizador_PK PRIMARY KEY (localizador),
	CONSTRAINT DNIPasajero_FK FOREIGN KEY (DNIPasajero) REFERENCES PASAJERO(DNI)
);

CREATE TABLE IF NOT EXISTS RESERVA_VUELO (
	localizadorReserva CHAR(6),
	codigoVuelo VARCHAR(7),
	CONSTRAINT localizadorReserva_FK FOREIGN KEY (localizadorReserva) REFERENCES RESERVA(localizador),
	CONSTRAINT codigoVuelo_FK FOREIGN KEY (codigoVuelo) REFERENCES VUELO(codigoVuelo)
);

SHOW TABLES FROM PRACTICA_DIEGO;