#include<stdio.h>
#include<stdlib.h>

#define LON 20

int main (int argc, char *argv[]) {
  unsigned char arraychar[LON];
  for (int i = 0; i < LON; i++)
    arraychar[i] = (i + 1) * (i + 1);

  for (int i = 0; i < LON; i++) 
    printf ("Celda: %2i --> Numero: %3i.\n", i, arraychar[i]);

//  printf ("%i\n", arraychar[0]);

  return EXIT_SUCCESS;
}
