#include<stdio.h>
#include<stdlib.h>
#define P(...) printf (__VA_ARGS__);

int main (int argc, char *argv[]) {
    int b = 73;
    b %= 5;
    P("73 %% 5 = %i\n", b);
    b <<= 2;
    P("3 << 2 = %i", b);
    P("\n");
    return EXIT_SUCCESS;
}
