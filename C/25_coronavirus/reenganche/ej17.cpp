#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
	/* Declaraci�n de variables */
	int *nums; /* Array destino de los scanf */
	int numofnums = -1; /* Conteo de n�meros, es -1 porque al entrar en el do-while le sumo 1 */
	int sum = 0; /* Suma de los elementos de array */
	double media; /* Media */
	
	nums = (int *) malloc (numofnums * sizeof(int)); /* Reservo memoria para el array */
	do {
		numofnums++; /* Sumo 1 a la longitud del array */
		nums = (int *) realloc (nums, numofnums * sizeof(int)); /* Aumento la memoria reservada para el array */
		printf ("Escribe un numero: ");
		scanf (" %i", &nums[numofnums]); /* Entrada de datos */
		for (int u = 0; u <= numofnums; u++) printf ("%i, ", nums[u]); /* Imprimo la lista (Decoraci�n) */
		printf ("\n");
		if (nums[numofnums] < 0) break; /* Si metes un n�mero negativo sales del bucle */
	} while (true);
	//	Si la lista tiene m�s de 0 elementos
	if (numofnums > 0) {
		/* Sumo todos los items del array */
		for (int j = 0; j < numofnums; j++)
			sum += nums[j];
		
		/* calculo la media con la suma y el numero de items */
		media = (double) sum / numofnums;
		
		/* Imprimo los resultados */
		printf ("La suma es: %i\n", sum);
		printf ("La media es: %lf\n", media);
	} else {
		printf ("Tienes que introducir al menos 1 numero.\n");
	}
	/*
	int mediana = nums[N / 2 - 1];
	printf ("Mediana: %i", mediana);*/

	
	return EXIT_SUCCESS;
}
