#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main (int argc, char *argv[]) {
  float c1, c2, h;
  int el;
  printf ("Que quieres calcular?\n1. Cateto.\n2. Hipotenusa.\n");
  scanf (" %d", &el);
  if (el == 1) {
    printf ("Cual es la medida de la hipotenusa?:");
    scanf (" %f", &h);
    printf ("Cual es la medida del cateto?");
    scanf (" %f", &c1);
    printf ("La medida del cateto es %f\n", (float) sqrt(pow(h, 2) - pow(c1, 2)));
  } else if (el == 2) {
     printf ("Cual es la medida del primer cateto?:");
    scanf (" %f", &c1);
    printf ("Cual es la medida del segundo cateto?");
    scanf (" %f", &c2);
    printf ("La medida del cateto es %55555.2f\n", (float) sqrt(pow(c1, 2) + pow(c2, 2)));
  } else {
    printf ("Esa no es una opcion válida\n");
    return EXIT_SUCCESS;
  } 
  return EXIT_SUCCESS;
}
