#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  int *p; // Declaras una variable con un adirección de memoria

  p = (int *) malloc (sizeof (int)); // Le asignas una capacidad maxima
  
  *p = 2; // Metes un número en esa dirección
  
  printf ("%i\n", *p);

  free (p); // Para limpiar p, y recuperar memoria
  return EXIT_SUCCESS;
}
