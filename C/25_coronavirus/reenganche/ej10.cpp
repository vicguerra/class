#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main (int argc, char *argv[]) {
	char color = 'B';
	printf ("Introduce la inicial de un color (r, g, b): ");
	scanf (" %c", &color);
	
	switch (tolower(color)) {
		case 'r':
			printf ("Has dicho 'r'\n");
			break;
		case 'g':
			printf ("Has dicho 'g'\n");
			break;
		case 'b':
			printf ("Has dicho 'b'\n");
			break;
		default:
			printf ("No valido\n");
	}
	return EXIT_SUCCESS;
}
