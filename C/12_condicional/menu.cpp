#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
    char op;
    int op1, op2;
    system ("clear");
    system ("toilet --filter border -fpagga --gay Calculadora");
    printf ("==================\n    s: Suma\n    r: Resta\n==================\n");
    printf ("Que operacion quieres?: ");
    scanf (" %c", &op);
    if (op != 's' && op != 'S' && op != 'r' && op != 'R') {
        fprintf (stderr, "Esa no es una operacion valida.\n");
        return EXIT_FAILURE;
    }
    printf ("Cual es el primer operando?: ");
    scanf (" %d", &op1);
    printf ("Cual es el segundo operando?: ");
    scanf (" %d", &op2);
    // Todo en uno (Solo funciona si hay dos operaciones)
    printf ("%i %c %i = %i\n", op1, op == 's' || op == 'S' ? '+' : '-', op2, op == 's' || op == 'S' ? (op1 + op2) : (op1 - op2));
    //printf ("%c - %i - %i", op, op1, op2);
    printf ("\n");
    return EXIT_SUCCESS;
}
