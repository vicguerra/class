/*
#################################
##       Diego Martin          ##
#################################
##     Cuadrado solo borde     ##
#################################
*/
#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
	int lado;
	char *car;
	printf ("Voy a imprimir un cuadrado.\nIntroduce un caracter: ");
	scanf ("%s", car);
	printf ("Cuanto quieres que mida su lado?: ");
	scanf ("%d", &lado);
	for (int i = 0; i < lado; i++) {
		for (int j = 0; j < lado; j++){
        if (i == 0 || j == 0 || i == lado - 1 || j == lado - 1) {
            printf ("%s", car);
        } else {
            printf (" ");
        }
    }
		printf ("\n");
	}
	printf ("\nHecho\n");
	return EXIT_SUCCESS;
}
