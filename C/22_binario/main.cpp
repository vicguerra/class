/*
 * Programa para pasar decimal a binario
 */
#include <stdio.h>
#include <stdlib.h>

void titulo () {
  system ("clear");
  system ("toilet -fpagga --gay Binario");
}

void invertir (int num) {
  if (num > 0) {
    invertir (num / 2);
    printf ("%i", (num % 2));
  }
}

int main (int argc, char *argv[]) {
  char *nombre;
  unsigned num;
  titulo ();
  // Preguntar el nombre al user (Decoro)
  printf ("Cual es tu nombre?: ");
  scanf (" %ms", &nombre); // Hace un free despues del malloc a lo que reciba en stdin
  // Saludar al user
  titulo ();
  printf ("Espero que estes teniendo un buen dia %s.\n", nombre);
  free (nombre); // vacias la variable 'nombre'
  // Pregunto un numero
  printf ("Que número quieres que pase a binario?: ");
  scanf (" %u", &num);
  // Lo paso a binario
  invertir (num);

  printf ("\n");
 
  return EXIT_SUCCESS;
}
