#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
  int base;
  unsigned exp;
  int res = 1, subres = 0;

  printf ("Base: ");
  scanf (" %i", &base);
  printf ("Exponente: ");
  scanf (" %u", &exp);

  for (unsigned i = 0; i < exp; i++) {
    subres = 0;
    for (unsigned j = 0; j < base; j++)
      subres += res;
    res = subres;
  }
  printf ("Resultado: %i ^ %u = %i.\n", base, exp, res);
  return EXIT_SUCCESS;
}
