#include<stdio.h>
#include<stdlib.h>

#define DIM 3

void title () {
	system ("clear");
	system ("toilet -fpagga --gay Multiplicar");
	printf ("\n\n");
}

void printMatriz (double dato[DIM][DIM], const char *nombre) {
	printf ("\tdouble %s[%i][%i] = {\n", nombre, DIM, DIM);
	for (int i = 0; i < DIM; i++) {
		printf ("\t\t{");
		for (int j = 0; j <DIM; j++) {
			if (j != (DIM - 1))
				printf ("%9.2lf, ", dato[i][j]);
			else
				printf ("%9.2lf", dato[i][j]);
		}
		if (i != (DIM - 1))
			printf ("},\n");
		else
			printf ("}\n");
	}
	printf ("\t};\n\n");
}

void pedirDatos (double datos[DIM][DIM]) {
	for (int i = 0; i < DIM; i++)
		for (int j = 0; j < DIM; j++) {
			printf ("\t                                                                      \r");
			printf ("\tIndroduce el valor de la celda (Y: %i, X: %i): ", i + 1, j + 1);
			scanf (" %lf", &datos[i][j]);
			printf ("\x1B[1A");
			fflush (stdout);
		}
	printf ("\t                                                                      \r");
	printf ("\t                                                                      \n");
	printf ("\x1B[1A");
	fflush (stdout);
}

int main (int argc, char *argv[]) {
	/* Creacion de variables */
	double matrizBase[DIM][DIM] = {
		{5, -2, 3},
		{-5, 5, 2},
		{2, -3, 5}
	};
	double matrizUser[DIM][DIM];
	double res[DIM][DIM];
	double mul = 1;
	/* Interfaz y recojida de datos */
	title ();
	pedirDatos (matrizUser);
	printMatriz (matrizBase, "matrizBase");
	printMatriz (matrizUser, "Tu matriz");
	printf ("\n\n----------------------------Resultado de la multiplilcación de las matrices--------------------------------\n\n");

	/* Calculo del resultado */
	for (int i = 0; i < DIM; i++)
		for (int j = 0; j < DIM; j++) {
			mul = 0;
			for (int k = 0; k < DIM; k++)
				mul += matrizBase[i][k] * matrizUser[k][j];
			res[i][j] = mul;
		}

	/* Muestro el resultado */
	printMatriz (res, "Matriz resultante");
    return EXIT_SUCCESS;
}
