DROP DATABASE IF EXISTS Ejer1Repaso;
CREATE DATABASE Ejer1Repaso;
USE Ejer1Repaso;

CREATE TABLE IF NOT EXISTS CIUDAD (
	codigo TINYINT UNSIGNED AUTO_INCREMENT,
	nombre VARCHAR(30),
	CONSTRAINT CIUDAD_PK PRIMARY KEY (codigo)
);

CREATE TABLE IF NOT EXISTS TEMPERATURA (
	dia DATE,
	codigo_CIUDAD TINYINT UNSIGNED,
	grados DECIMAL(3,1) NOT NULL,
	CONSTRAINT TEMPERATURA_PK PRIMARY KEY (dia, codigo_CIUDAD),
	CONSTRAINT TEMPERATURA_FK FOREIGN KEY (codigo_CIUDAD) REFERENCES CIUDAD(codigo)
);

CREATE TABLE IF NOT EXISTS HUMEDAD (
	dia DATE,
	codigo_CIUDAD TINYINT UNSIGNED,
	grados DECIMAL(3,1) NOT NULL,
	CONSTRAINT HUMEDAD_PK PRIMARY KEY (dia, codigo_CIUDAD),
	CONSTRAINT HUMEDAD_FK FOREIGN KEY (codigo_CIUDAD) REFERENCES CIUDAD(codigo)
);

CREATE TABLE IF NOT EXISTS INFORME (
	id INT(5),
	dia DATE,
	codigo_CIUDAD TINYINT UNSIGNED,
	CONSTRAINT INFORME_PK PRIMARY KEY (dia, codigo_CIUDAD),
	CONSTRAINT INFORME_FK FOREIGN KEY (codigo_CIUDAD) REFERENCES CIUDAD(codigo)
);