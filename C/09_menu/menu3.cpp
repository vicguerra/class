#include<stdio.h>
#include<stdlib.h>
#include<math.h>

enum ops {
    suma,
    resta,
    multiplicacion,
    division,
    modulo,
    potencia,
    TOTALOPCIONES
};
const char *opciones[] = {
    "Suma", 
    "Resta", 
    "Multiplicacion",
    "Division",
    "Resto",
    "Potencia",
    NULL
};

int menu () {
    int opcion;
    system ("clear");
    system ("toilet -fpagga --gay Calculadora");
    printf ("\n\
----------------------\n\
         Menu\n\
----------------------\n\
");
    for (int o = 0; o < TOTALOPCIONES; o++)
        printf ("%i.- %s\n", o + 1, opciones[o]);

    printf("----------------------\n\
Opcion: ");
    scanf (" %i", &opcion);

    return opcion - 1;
}

int main (int argc, char *argv[]) {
    int op1 = 2, op2 = 5;
    char *p;
    if (argc > 2 && argc < 4) {
        op1 = strtol(argv[1], &p, 10);
        op2 = strtol(argv[2], &p, 10);
    }  
    int opcion = menu ();
    if (opcion < TOTALOPCIONES && opcion > 0)
       printf ("Has elegido: %s\n", opciones[opcion]);

    switch (opcion) {
      case suma:
        printf ("%i + %i = %i\n", op1, op2, (op1 + op2));
        break;
      case resta:
        printf ("%i - %i = %i\n", op1, op2, (op1 - op2));
        break;
      case multiplicacion:
        printf ("%i * %i = %i\n", op1, op2, (op1 * op2));
        break;
      case division:
        printf ("%i / %i = %i\n", op1, op2, (op1 / op2));
        break;
      case modulo:
        printf ("%i %% %i = %i\n", op1, op2, (op1 % op2));
        break;
      case potencia:
        printf ("%i ^ %i = %i\n", op1, op2, (int)pow(op1, op2));
        break;
      default:
        fprintf (stderr, "Esa no es una opcion valida.");
        break;
    }
    printf ("\n");
    return EXIT_SUCCESS;
}
