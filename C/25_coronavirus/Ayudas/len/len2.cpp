#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char* argv[]) {
	char *nums = "0123456789";
	char *miString = "H0l4 que t4l 3st4s";
	int contador = 0;
	
	for (int i = 0; i < strlen(miString); i++)
		for (int j = 0; j < strlen(nums); j++)
			if (miString[i] == nums[j])
				contador++;
	
	printf ("%i\n", contador);
	return EXIT_SUCCESS;
}
