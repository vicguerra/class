#include<stdio.h>
#include<stdlib.h>

float f (float x) { return x * x -3; }

int main (int argc, char *argv[]) {
    printf ("Puntos desde x = -5 hasta x = 5:\n");
    for (float x = -5; x <= 5; x += 0.5)
        printf ("x: %.2lf, y: %.2lf\n", x, f(x));
    printf ("\n");
    return EXIT_SUCCESS;
}
