#include<stdio.h>

int main () {
	long long int num, sum = 0;
	printf("=======================\n");
	printf("       Sumatorio       \n");
	printf("=======================\n");
	printf("Introduce un numero:");
	scanf(" %ld", &num);
	printf("\n");
	while (num > 0) {
		sum += num;
		num--;
	}
	printf("El resultado es: %i\n", sum);
	return 0;
}
