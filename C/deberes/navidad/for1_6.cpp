#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  int ancho, alto;
  printf ("Cual es el ancho del rectangulo?: ");
  scanf (" %d", &ancho);
  printf ("Cual es el alto del rectangulo?: ");
  scanf (" %d", &alto);
  printf ("\n");
  for (int i = 0; i < alto; i++) {
    for (int j = 0; j < ancho; j++)
      printf ("*");
    printf ("\n");
  }
  return EXIT_SUCCESS;
}
