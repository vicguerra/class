function TiposFijos() 
{
	var parrafoVariable,
	    nombre1 = "Ana Ruiz",
	    nombre2 = "Juan Pérez",
	    numero1 = 23,
	    numero2 = 45
    /* Con esta instrucción accedemos al párrafo de HTML
       cuyo ID es resultadoFijo. Una vez almacenamos este
       párrafo en la variable parrafoVariable podemos cambiar 
       cualquier de sus propiedades. Como por ejemplo, la
       propiedad innerHTML 
    */
	parrafoVariable = document.getElementById('resultadoFijo')
	parrafoVariable.innerHTML = "Variables de tipo fijo <br>";
	parrafoVariable.innerHTML += " Tipo String <br>";
	parrafoVariable.innerHTML += "Nombre1 es: " + nombre1 + "<br>"
	parrafoVariable.innerHTML += "Nombre2 es: " + nombre2 + "<br>"
	parrafoVariable.innerHTML += "nombre1+nombre2 es: " + 
	                         (nombre1+nombre2) + "<br>"
	parrafoVariable.innerHTML += "Variables de tipo fijo <br>";
	parrafoVariable.innerHTML += " Tipo Integer  <br>";
	parrafoVariable.innerHTML += "Numero1 es: " + numero1 + "<br>"
	parrafoVariable.innerHTML += "Numero2 es: " + numero2 + "<br>"
	parrafoVariable.innerHTML += "numero1+numero2 es: " + 
	                         (numero1+numero2) + "<br>"
}
function TiposVariables() 
{
	var parrafoVariable,
	    variable1 = "Ana Ruiz",
	    variable2 = "Juan Pérez",

    /* Con esta instrucción accedemos al párrafo de HTML
       cuyo ID es resultadoVariable. Una vez almacenamos este
       párrafo en la variable parrafoVariable podemos cambiar 
       cualquier de sus propiedades. Como por ejemplo, la
       propiedad innerHTML 
    */
	parrafoVariable = document.getElementById('resultadoVariable')
	parrafoVariable.innerHTML = "Variables de tipo variable <br>";
	parrafoVariable.innerHTML += " Tipo String <br>";
	parrafoVariable.innerHTML += "Variable1 es: " + variable1 + "<br>"
	parrafoVariable.innerHTML += "Variable2 es: " + variable2 + "<br>"
	parrafoVariable.innerHTML += "variable1+variable2 es: " + 
	                         (variable1+variable2) + "<br>"
	parrafoVariable.innerHTML += "variable1*variable2 es: " + 
	                         (variable1*variable2) + "<br>"
	variable1 = 124;         
	parrafoVariable.innerHTML += "Variables de tipo variable <br>";
	parrafoVariable.innerHTML += "Variable1 es: " + variable1 + "<br>"
	parrafoVariable.innerHTML += "Variable2 es: " + variable2 + "<br>"
	parrafoVariable.innerHTML += "variable1+variable2 es: " + 
	                         (variable1+variable2) + "<br>"
	parrafoVariable.innerHTML += "variable1*variable2 es: " + 
	                         (variable1*variable2) + "<br>"
}