#include<stdio.h>
#define AMARILLO "\x1b[33m"
#define BLANCO   "\x1b[0m"

int main() {
  // imprimo un tabluador en blanco
  printf(" \t");
  // imprimo las cifras en hexadecimal (los nombres de las columnas de la tabla)
  for (int x = 0x00; x < 0x10; x++) printf(AMARILLO "%X\t", x);
  printf("\n");
  // El primer for imprime las filas
  for (int fila = 0x02; fila < 0x08; fila++) {
    // Este  printf imprime los nombres de las filas
    printf(AMARILLO "%i\t", fila);
    // Este segundo for imprime las celdas de la fila por la que vas
    for (int col = 0x00; col < 0x10; col++) {
      // Obtengo el número en múltiplo de 16 de la fila y le sumo la columna, esta sera la posición del caracter en la tabla
      int cel = (fila * 16) + col;
      // La posición del caracter en la tabla si lo pasas a hexadecimal, es el código del caracter
      int car = ("0x%X", cel);
      // Imprimo el caracter
      printf(BLANCO "%c\t", car);
    }
    printf("\n");
  }
  return 0;
}
