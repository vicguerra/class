#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAXLIN 0x100

void cifra (char *frase, int clave) {
  while (*frase != '\0')
    *(frase++) += clave;
}

void concatena (char *dest, const char *org, int max) {
    /* 
     * Avanzar destino hasta \0
     * Copiar caracter a caracter de 'org' a 'dest' como mucho 'max' hasta \0
     */
    while (*dest != '\0') dest++;
    for (int c = 0; *org != '\0' && c < max; c++, ++org, ++dest)
      *dest = *org;
    /*
     * ------ Alternativas ------
     * for (int c = 0; *org != '\0' && c < max; c++, ++org)
     *   *(dest + c) = *org;
     * for (int c = 0; *org != '\0' && c < max; c++, ++org)
     *   dest[c] = *org;
     */
    *dest = *org;
}

const char * const cprog = "cadenas";
char vprog[] = "programas";

int main (int argc, char *argv[]) {
  char linea[MAXLIN];
  strcpy (linea, cprog);
  strncpy (linea, cprog, MAXLIN);
  strcat (linea, " ");
//  strncat (linea, " ", MAXLIN);

  concatena (linea, cprog, MAXLIN - strlen(linea));
  cifra (linea + strlen (linea) + 1, 3);
  printf ("%s\n", linea);
  printf ("Secreto: %s\n", linea + strlen (linea) + 1);
  return EXIT_SUCCESS;
}
