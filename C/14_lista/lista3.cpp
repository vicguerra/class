#include<stdio.h>
#include<stdlib.h>

const char *lista[] = {"Good morning", "Gutten morgen"};

int main (int argc, char *argv[]) {
  /* (sizeof(lista) / sizeof(char *)) divide los bytes de la lista entre los bytes del tipo de dato que es, de manera que devuelve la longitud del array */
  printf ("Bytes de lista: %lu\nCeldas de lista: %lu\n", sizeof(lista), sizeof(lista) / sizeof(char *));
  for (int i = 0; i < sizeof(lista) / sizeof(char *); i++) printf ("Elemento %i: %s\n", i, lista[i]); /* Imprime */
  return EXIT_SUCCESS;
}
