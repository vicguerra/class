#include <stdio.h>
#include <stdlib.h>
#define N 8

int main (int argc, char *argv[]) {
	int nums[N];
	int sum = 0;
	
	for (int i = 0; i < N; i++) {
		printf ("Escribe un numero: ");
		scanf (" %i", &nums[i]);
		
	}
	
	for (int j = 0; j < N; j++)
		sum += nums[j];
	
	printf ("La suma es: %i\n", sum);
	
	int mediana = nums[N / 2 - 1];
	printf ("Mediana: %i", mediana);

	
	return EXIT_SUCCESS;
}
