-- Ejercicio 1
SELECT CodigoOficina, Ciudad FROM oficinas;

-- Ejercicio 2
SELECT COUNT(*) AS "Numero de empleados" FROM empleados;

-- Ejercicio 3
SELECT COUNT(CodigoCliente) AS "Numero de clientes", Pais FROM clientes GROUP BY Pais;

-- Ejercicio 4
SELECT AVG(Cantidad) AS "Pago Medio" FROM pagos WHERE FechaPago BETWEEN '2007-12-31' AND '2009-01-01';

-- Ejercicio 5
SELECT COUNT(CodigoPedido) AS "Numero de pedidos", Estado FROM pedidos GROUP BY Estado ORDER BY COUNT(CodigoPedido) DESC;

-- Ejercicio 6
SELECT MAX(PrecioVenta) AS "Producto mas caro", MIN(PrecioVenta) AS "Producto mas barato" FROM productos;

-- Ejercicio 7
SELECT Ciudad, Telefono FROM oficinas WHERE Pais = "EEUU";

-- Ejercicio 8
SELECT Nombre, CONCAT(Apellido1, " ", Apellido2) AS Apellidos, Email FROM empleados WHERE CodigoJefe = (SELECT CodigoEmpleado FROM empleados WHERE Nombre = "Alberto" AND Apellido1 = "Soria");

-- Ejercicio 9
SELECT Puesto, CONCAT(Nombre, " ", Apellido1, " ", Apellido2) AS "Nombre completo", Email FROM empleados WHERE CodigoJefe IS NULL;

-- Ejercicio 10
SELECT CONCAT(Nombre, " ", Apellido1, " ", Apellido2) AS "Nombre completo", Puesto FROM empleados WHERE Puesto != "Representante Ventas";

-- Ejercicio 11
SELECT COUNT(*) AS "Numero de Clientes" FROM Clientes;

-- Ejercicio 12
SELECT NombreCliente FROM Clientes WHERE Pais = "España";

-- Ejercicio 13
SELECT COUNT(CodigoCliente) AS "Total de clientes", Pais FROM Clientes GROUP BY Pais; 

-- Ejercicio 14
SELECT COUNT(CodigoCliente) AS "Total de clientes", Ciudad FROM Clientes WHERE Ciudad = "Madrid" AND Pais = "Spain";

-- Ejercicio 15
SELECT Ciudad, COUNT(*) AS "Numero de ciudades" FROM Clientes WHERE Ciudad LIKE "M%" GROUP BY Ciudad;

-- Ejercicio 16
SELECT CodigoEmpleadorepVentas AS "Representante de Ventas", COUNT(*) AS "Clientes Atendidos" FROM Clientes GROUP BY CodigoEmpleadoRepVentas;

-- Ejercicio 17
SELECT CodigoCliente, COUNT(*) AS "NºClientes" FROM Clientes WHERE CodigoEmpleadoRepVentas IS NULL GROUP BY CodigoCliente;

-- Ejercicio 18
SELECT CodigoCliente AS "Cliente", MAX(Cantidad) AS "Pago maximo", MIN(Cantidad) AS "Pago minimo" FROM pagos;

-- Ejercicio 18 Mejorado
(SELECT * FROM pagos ORDER BY FechaPago ASC LIMIT 1) UNION (SELECT * FROM pagos ORDER BY FechaPago DESC LIMIT 1);

-- Ejercicio 19
SELECT CodigoCliente FROM pagos WHERE FechaPago BETWEEN '2007-12-31' AND '2009-01-01';

-- Ejercicio 20
SELECT DISTINCT estado FROM pedidos;

-- Ejercicio 21
SELECT CodigoPedido, CodigoCliente, Estado, FechaEsperada, FechaEntrega FROM Pedidos WHERE DATE(FechaEntrega) IS NOT NULL AND DATE(FechaEntrega) > DATE(FechaEsperada) AND (Estado = 'Entregado' OR Estado = 'Rechazado');

-- Ejercicio 22
SELECT COUNT(*) AS "Numero de productos", NumeroLinea AS "Numero de linea" FROM detallepedidos GROUP BY NumeroLinea;

-- Ejercicio 23
SELECT Cantidad AS "Numero de productos", CodigoPedido AS "Codigo del pedido" FROM detallepedidos GROUP BY CodigoPedido ORDER BY Cantidad DESC LIMIT 20;

-- Ejercicio 24
SELECT CodigoPedido, CodigoCliente, FechaEsperada, FechaEntrega, DATEDIFF(FechaEntrega, FechaEsperada) AS "Dias de diferencia" FROM Pedidos WHERE FechaEntrega IS NOT NULL AND FechaEsperada IS NOT NULL AND DATEDIFF(FechaEntrega, FechaEsperada) >= 2;

-- Ejercicio 25
SELECT SUM(PrecioUnidad * Cantidad) AS "Base Imponible", SUM(PrecioUnidad * Cantidad) * 0.21 AS IVA, SUM(PrecioUnidad * Cantidad) * 1.21 FROM detallepedidos;

-- Ejercicio 26
SELECT SUM(PrecioUnidad * Cantidad) AS "Base Imponible", SUM(PrecioUnidad * Cantidad) * 0.21 AS IVA, SUM(PrecioUnidad * Cantidad) * 1.21 FROM detallepedidos WHERE CodigoProducto LIKE "FR%" GROUP BY CodigoProducto;

-- Ejercicio 27
SELECT Nombre AS "Producto mas caro" FROM productos ORDER BY PrecioVenta LIMIT 1;

-- Ejercicio 28
SELECT Nombre FROM productos WHERE CodigoProducto = (SELECT CodigoProducto FROM detallepedidos GROUP BY CodigoProducto ORDER BY COUNT(*) DESC LIMIT 1);

-- Ejercicio 29


-- Ejercicio 30
(select Nombre, MAX(CantidadEnStock) as 'Maximo y Minimo' from Productos group by Nombre order by MAX(CantidadEnStock) desc limit 1) union (select Nombre, MIN(CantidadEnStock) from Productos group by Nombre order by MIN(CantidadEnStock) limit 1);