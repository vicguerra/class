#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
  char frase[] = "El sol esta muy alto en el cielo";
  char *punt;
  int i;
//  printf ("%lu\n", sizeof(frase));
  for (i = 0; frase[i] != 'u' && frase[i] != '\0'; i++) punt = &frase[i];

  (frase[i] == 'u') ? printf ("La letra 'u' esta en la posicion %i.\nSu direccion en memoria es la %p\n", i, punt) : printf ("No hay letra 'u' en esa cadena.\n");

  return EXIT_SUCCESS;
}
