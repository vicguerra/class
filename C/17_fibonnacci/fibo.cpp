#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

#define MAX 0x100

int main (int argc, char *argv[]) {
  int i; // Iterador
  uint64_t lim;
  uint64_t f[MAX]; // Número al que se aplica la operación
  
  lim = 1UL << ((sizeof (uint64_t) - 1) * 8);

  f[0] = 1;
  f[1] = 1;

  for (i = 2; i < MAX && f[i - 1] < lim; i++)
    f[i] = f[i - 1] + f[i - 2];

  for (int x = 1; x < i && x < MAX; x++)
    printf ("%4i: %19lu [%9.6lf]\n", x, f[x], (double) f[x] / f[x - 1]);

  return EXIT_SUCCESS;
}
