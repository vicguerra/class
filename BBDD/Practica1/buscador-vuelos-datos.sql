USE PRACTICA_DIEGO;

-- Añado 4 aeropuertos al azar
INSERT INTO AEROPUERTO (`codigoIATA`, `nombre`, `ciudad`, `pais`) VALUES ('MAD', 'Adolfo Suarez Madrid - Barajas', 'Madrid', 'España');
INSERT INTO AEROPUERTO (`codigoIATA`, `nombre`, `ciudad`, `pais`) VALUES ('TOJ', 'Base Aérea de Torrejón de Ardoz', 'Madrid', 'España');
INSERT INTO AEROPUERTO (`codigoIATA`, `nombre`, `ciudad`, `pais`) VALUES ('FDO', 'Aeropuerto Internaciones de San Fernando', 'Buenos Aires', 'Argentina');
INSERT INTO AEROPUERTO (`codigoIATA`, `nombre`, `ciudad`, `pais`) VALUES ('HND', 'Aeropuerto Internacional de Haneda', 'Tokio', 'Japón');
INSERT INTO AEROPUERTO (`codigoIATA`, `nombre`, `ciudad`, `pais`) VALUES ('LAS', 'Las Vegas, Mc Carran International Airport', 'California', 'Estados Unidos');
SELECT * FROM AEROPUERTO;

-- Añado algunas compañias inventadas o no
INSERT INTO COMPANIA (`codigoCompania`, `nombre`, `logo`) VALUES ('IB', 'Iberia Líneas Aéreas de España', 'https://www.iberia.com/wcs/imagenes/otros/iberia-rrss.png');
INSERT INTO COMPANIA (`codigoCompania`, `nombre`, `logo`) VALUES ('DM', 'Diego Aviones España S.L.', 'https://i.pinimg.com/originals/4e/d4/ff/4ed4ff419a76db276e2562e5eb53c920.png');
INSERT INTO COMPANIA (`codigoCompania`, `nombre`, `logo`) VALUES ('FM', 'Fernando Aviones S.L.', 'https://icons.iconarchive.com/icons/iconarchive/red-orb-alphabet/256/Letter-F-icon.png');
INSERT INTO COMPANIA (`codigoCompania`, `nombre`, `logo`) VALUES ('MS', 'Egyptair', 'https://media.united.com/images/Media%20Database/SDL/MileagePlus%20Partners/airlines/egyptair-v2.jpg');
INSERT INTO COMPANIA (`codigoCompania`, `nombre`, `logo`) VALUES ('UX', 'Air Europa Lineas Aereas', 'https://www.barcelona-airport.com/images/airlines17/air-europa.png');

-- Añado 3 terminales a cada aeropuerto
INSERT INTO TERMINAL (`numero`, `codigoIATA`) VALUES (1, 'MAD'), (2, 'MAD'), (3, 'MAD'), (5, 'MAD');
INSERT INTO TERMINAL (`numero`, `codigoIATA`) VALUES (1, 'TOJ'), (2, 'TOJ'), (3, 'TOJ');
INSERT INTO TERMINAL (`numero`, `codigoIATA`) VALUES (1, 'FDO'), (2, 'FDO'), (3, 'FDO');
INSERT INTO TERMINAL (`numero`, `codigoIATA`) VALUES (1, 'HND'), (2, 'HND'), (3, 'HND'), (23, 'HND'), (24, 'HND'), (25, 'HND');
SELECT * FROM TERMINAL;

-- Actualizo la terminal 5 de Madrid - Barajas y la pongo en 4
-- Y algunas mal introducidas del Aeropuerto Internacional de Haneda
UPDATE TERMINAL SET numero = 4 WHERE codigoIATA = 'MAD' AND numero = 5;
UPDATE TERMINAL SET numero = 4 WHERE codigoIATA = 'HND' AND numero = 23;
UPDATE TERMINAL SET numero = 5 WHERE codigoIATA = 'HND' AND numero = 24;
UPDATE TERMINAL SET numero = 6 WHERE codigoIATA = 'HND' AND numero = 25;
SELECT * FROM TERMINAL;

-- Borro algunas terminales que no existen
DELETE FROM TERMINAL WHERE codigoIATA = 'TOJ' AND numero = 2;
DELETE FROM TERMINAL WHERE codigoIATA = 'TOJ' AND numero = 3;
DELETE FROM TERMINAL WHERE codigoIATA = 'FDO' AND numero = 3;

-- Añado dos vuelos
INSERT INTO VUELO (`codigoVuelo`, `codigoCompania`, `aeropuertoOrigen`, `aeropuertoDestino`, `estado`, `fecha`) VALUES ('1234567', 'DM', 'TOJ', 'HND', 'En espera', '2020-04-19 16:00:00');
INSERT INTO VUELO (`codigoVuelo`, `codigoCompania`, `aeropuertoOrigen`, `aeropuertoDestino`, `estado`, `fecha`) VALUES ('0123456', 'FM', 'HND', 'FDO', 'En curso', '2020-03-21 09:00:00');

-- Añado asientos en los vuelos
INSERT INTO ASIENTO (`codigoAsiento`, `codigoVuelo`, `tipoClase`) VALUES ('01A', '1234567', 'Pobre');
INSERT INTO ASIENTO (`codigoAsiento`, `codigoVuelo`, `tipoClase`) VALUES ('11A', '1234567', 'Turista');
INSERT INTO ASIENTO (`codigoAsiento`, `codigoVuelo`, `tipoClase`) VALUES ('11B', '1234567', 'Turista');
INSERT INTO ASIENTO (`codigoAsiento`, `codigoVuelo`, `tipoClase`) VALUES ('02A', '0123456', 'Pobre');
INSERT INTO ASIENTO (`codigoAsiento`, `codigoVuelo`, `tipoClase`) VALUES ('12C', '0123456', 'Turista');
INSERT INTO ASIENTO (`codigoAsiento`, `codigoVuelo`, `tipoClase`) VALUES ('14F', '0123456', 'Turista');

-- Añado un par de personas
INSERT INTO PASAJERO (`DNI`, `nombre`, `apellido1`, `apellido2`) VALUES ('56051380G', 'Mireia', 'Belmonte', 'García');
INSERT INTO PASAJERO (`DNI`, `nombre`, `apellido1`, `apellido2`) VALUES ('45615314K', 'Antonio', 'Dominguez', 'Bandera');
INSERT INTO PASAJERO (`DNI`, `nombre`, `apellido1`, `apellido2`) VALUES ('34972861R', 'Fernando', 'Alonso', 'Diaz');
INSERT INTO PASAJERO (`DNI`, `nombre`, `apellido1`, `apellido2`) VALUES ('64257315W', 'Elsa', 'Lafuente', 'Medianu');

-- Hacen las reservas
INSERT INTO RESERVA (`localizador`, `DNIPasajero`, `precio`, `fecha`) VALUES ('234567', '56051380G', 25.46, (SELECT NOW()));
INSERT INTO RESERVA (`localizador`, `DNIPasajero`, `precio`, `fecha`) VALUES ('456789', '45615314K', 25.46, (SELECT NOW()));
INSERT INTO RESERVA (`localizador`, `DNIPasajero`, `precio`, `fecha`) VALUES ('345678', '34972861R', 25.46, (SELECT NOW()));
INSERT INTO RESERVA (`localizador`, `DNIPasajero`, `precio`, `fecha`) VALUES ('567890', '64257315W', 25.46, (SELECT NOW()));

-- Relacionamos la reserva con su vuelo
INSERT INTO RESERVA_VUELO (`localizadorReserva`, `codigoVuelo`) VALUES ('345678', '1234567');
INSERT INTO RESERVA_VUELO (`localizadorReserva`, `codigoVuelo`) VALUES ('456789', '0123456');
INSERT INTO RESERVA_VUELO (`localizadorReserva`, `codigoVuelo`) VALUES ('567890', '1234567');
INSERT INTO RESERVA_VUELO (`localizadorReserva`, `codigoVuelo`) VALUES ('234567', '0123456');