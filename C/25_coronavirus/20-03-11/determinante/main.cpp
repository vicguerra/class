#include<stdio.h>
#include<stdlib.h>

#define DIM 3

void title () {
	system ("clear");
	system ("toilet -fpagga --gay Determinante");
	printf ("\n\n");
}

void printMatriz (double dato[DIM][DIM]) {
	printf ("\t{\n");
	for (int i = 0; i < DIM; i++) {
		printf ("\t\t{");
		for (int j = 0; j < DIM; j++) {
			if (j != (DIM - 1))
				printf ("%.2lf, ", dato[i][j]);
			else
				printf ("%.2lf", dato[i][j]);
		}
		if (i != (DIM - 1))
			printf ("},\n");
		else
			printf ("}\n");
	}
	printf ("\t}");
}

int quedateEn3 (int num) {
	return num % DIM;
}

int main (int argc, char *argv[]) {
	// Declaracion de variables
	double miMatriz[DIM][DIM] = { // No escribo nada en los corchetes porque el preprocesador lo calcula
		{1, 4, 7},
		{2, 5, 8},
		{3, 6, 9}
	};
	// Decoración
	title ();
	printMatriz (miMatriz);
	printf ("\n\n");
	// Calculos
	double res = 0, subres = 1;
	// Calculamos las diagonales principales
	for (int dpx = 0; dpx < DIM * 2; dpx++) {
		subres = 1;
		for (int dpy = 0; dpy < DIM * 2; dpy++) { // dpy es 'Diagonal principal en y'
			printf ("%lf * %lf = ", subres, miMatriz[quedateEn3(dpx)][quedateEn3(dpy)]);
			subres *= miMatriz[quedateEn3(dpx)][quedateEn3(dpy)];
			printf ("%lf\n", subres);
		}
		printf ("%lf + %lf = ", res, subres);
		res += subres;
		printf ("%lf\n", res);
	}
	printf ("%lf", res);
	printf ("\n\n");	
    return EXIT_SUCCESS;
}
