#include<stdio.h>
#include<stdlib.h>
#include<typeinfo>
using namespace std;

namespace cpl {
	class ent {
		public:
			ent (int set = NULL) {
				valor = set;
			}
			bool inc (int val = 1) {
				if (val != NULL) {
					if (valor += val) {
						return true;
					} else {
						printf ("\nError 1: Fallo en la asignacion de la variable\n");
						exit (EXIT_FAILURE);
					}
				} else {
					printf ("\nError 1.1: El metodo 'inc' no puede tener el parametro nulo (NULL)\n");
					exit (EXIT_FAILURE);
				}
			}
			bool dec (int val = 1) {
				if (val != NULL) {
					if (valor -= val) {
						return true;
					} else {
						printf ("\nError 2: Fallo en la asignacion de la variable\n");
						exit (EXIT_FAILURE);
					}
				} else {
					printf ("\nError 2.1: El metodo 'dec' no puede tener el parametro nulo (NULL)\n");
					exit (EXIT_FAILURE);
				}
			}
			bool sum (int val = 0) {
				if (val != NULL) {
					if (valor += val) {
						return true;
					} else {
						printf ("\nError 3: Fallo en la asignacion de la variable\n");
						exit (EXIT_FAILURE);
					}
				} else {
					printf ("\nError 3.1: El metodo 'sum' no puede tener el parametro nulo (NULL)\n");
					exit (EXIT_FAILURE);
				}
			}
			bool res (int val = 0) {
				if (val != NULL) {
					if (valor -= val) {
						return true;
					} else {
						printf ("\nError 4: Fallo en la asignacion de la variable\n");
						exit (EXIT_FAILURE);
					}
				} else {
					printf ("\nError 4.1: El metodo 'res' no puede tener el parametro nulo (NULL)\n");
					exit (EXIT_FAILURE);
				}
			}
			bool mul (int val = 1) {
				if (val != NULL) {
					if (valor *= val) {
						return true;
					} else {
						printf ("\nError 5: Fallo en la asignacion de la variable\n");
						exit (EXIT_FAILURE);
					}
				} else {
					printf ("\nError 5.1: El metodo 'mul' no puede tener el parametro nulo (NULL)\n");
					exit (EXIT_FAILURE);
				}
			}
			bool set (int val = NULL) {
				if (val != NULL) {
					if (valor = val) {
						return true;
					} else {
						printf ("\nError 6: Fallo en la asignacion de la variable\n");
						exit (EXIT_FAILURE);
					}
				} else {
					printf ("\nError 6.1: El metodo 'set' debe tener un parametro.\n");
					exit (EXIT_FAILURE);
				}
			}
			ent operator + (const ent &op) {
				this->valor += op.valor;
				return *this;
			}
			ent& operator++ () {
				this->valor++;
				return *this;
			}
			//ent& operator++ (long long int) {
			//	ent tmp = *this;
			//	++*this->valor;
			//	return *tmp;
			//}
			ent operator - (const ent &op) {
				this->valor -= op.valor;
				return *this;
			}
			ent& operator-- () {
				this->valor--;
				return *this;
			}
			ent operator * (const ent &op) {
				this->valor *= op.valor;
				return *this;
			}
			ent operator / (const ent &op) {
				this->valor += op.valor;
				return *this;
			}
			ent operator % (const ent &op) {
				this->valor %= op.valor;
				return *this;
			}
			bool operator < (const ent &op) {
				if (this->valor < op.valor)
					return false;
				else
					return true;
			}
			bool operator > (const ent &op) {
				if (this->valor > op.valor)
					return false;
				else
					return true;
			}
			bool operator == (const ent &op) {
				if (this->valor == op.valor)
					return false;
				else
					return true;
			}
			ent operator = (const ent &op) {
				this->valor = op.valor;
				return *this;
			}
			void print (const char *fin = "\n") {
				if (valor != NULL)
					printf ("%lli%s", valor, fin);
				else {
					printf ("Error 0: variable vacia\n");
					exit (EXIT_FAILURE);
				}
			}
		private:
			long long int valor;
	};

	//template <class Type>
	//char *type (Type var) {
	//	char *res;
	//	if (typeid(var) == typeid(ent))
	//		res = "<ent>";
	//	else
	//		res = "Nada";
	//	return res;
	//}
}
int main (int argc, char *argv[]) {
	cpl::ent num1(2); // Arreglar! No funcionan los numeros negativos
	cpl::ent num2(3);
	cpl::ent num3 = num1 + num2; // Arreglar! Al hacer print() de num1 aparece el de num3
	
	num1.inc(5);
	num1.print();
	//num1.print(", ");
	
	//num1.inc(2);
	
	//num1.print(", ");
	
	//num1.inc(6);
	
	//num1.print(", ");
	
	//num1.inc(70);
	
	//num1.print(", ");
	
	//num1.set(50);
	
	//num1.print(", ");
	
	//num1.mul(2);
	
	//num1.print();
	// char *tipo = cpl::type(num);
	// printf ("%s\n", tipo);
	
	return EXIT_SUCCESS;
}
