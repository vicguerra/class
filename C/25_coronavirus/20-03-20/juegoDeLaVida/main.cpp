#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <unistd.h>

#define LIBRE " "
#define OCUPADO "D" 

const char *itocc (int num) {
	switch (num) {
		case 0: return "0";
		case 1: return "1";
		case 2: return "2";
		case 3: return "3";
		case 4: return "4";
		case 5: return "5";
		case 6: return "6";
		case 7: return "7";
		case 8: return "8";
		case 9: return "9";
	}
}

int main (int argc, char *argv[]) {
	int width, height, cont = 0, filas, columnas, desecho;
	initscr ();
	int *casillas;
	while (cont < 10) {
		getmaxyx (stdscr, height, width);
		filas = height - 3;
		columnas = width - 3;
		// Reservamos celdas en un array bidimensional
		if (cont == 0) {
			casillas = (int *) malloc (columnas * filas * sizeof (int));
			for (int i = 0; i < filas; i++)
				for (int j = 0; j < columnas; j++)
					*(casillas + i * columnas + j) = 0;
		} else {
			casillas = (int *) realloc (casillas, columnas * filas * sizeof (int));
		}


		// Dibuja los caracteres
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				desecho = x;
				if (y == 0)
					mvprintw (y, (x + 3), itocc (desecho / 100));
				if (y == 1)
					mvprintw (y, (x + 3), itocc ((desecho - (desecho / 100)) / 10));
				if (y == 2)
					mvprintw (y, (x + 3), itocc (desecho - ((desecho - (desecho / 100)) / 10) / 10));
				if (*(casillas + y * width + x) == 0)
					mvprintw ((y + 3), (x + 3), LIBRE);
				else
					mvprintw ((y + 3), (x + 3), OCUPADO);
			}
		}

		// Sigo con el código
		refresh ();
		sleep (1);
		cont++;
	}
	free (casillas);
	endwin ();
	return 0;
}
