#ifndef __INTERFAZ_H__
#define __INTERFAZ_H__
#include "common.h"

#ifdef __cplusplus
extern "C" {
#endif
	void titulo ();
	void printOptions (const char *const estadio, enum TBase elegida = BASES);
	enum TBase preguntaOpt (enum TBase exception = BASES);
	void menu (enum TBase *src, enum TBase *dst);
	void preguntaVector (enum TBase base, double vector[DIM]);
#ifdef __cplusplus
}
#endif

#endif