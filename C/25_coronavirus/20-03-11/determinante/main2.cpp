#include<stdio.h>
#include<stdlib.h>

#define DIM 3

void title () {
	system ("clear");
	system ("toilet -fpagga --gay Determinante");
	printf ("\n\n");
}

void printMatriz (double dato[DIM][DIM]) {
	printf ("\tmiMatriz[%i][%i] = {\n", DIM, DIM);
	for (int i = 0; i < DIM; i++) {
		printf ("\t\t{");
		for (int j = 0; j < DIM; j++) {
			if (j != (DIM - 1))
				printf ("%.2lf, ", dato[i][j]);
			else
				printf ("%.2lf", dato[i][j]);
		}
		if (i != (DIM - 1))
			printf ("},\n");
		else
			printf ("}\n");
	}
	printf ("\t}");
}

int quedateEn3 (int num) {
	return num % DIM;
}

int sePositivo (int num) {
	if (num < 0)
		return num + DIM;
	return num;
}

int main (int argc, char *argv[]) {
	// Declaracion de variables
	double miMatriz[DIM][DIM] = { // No escribo nada en los corchetes porque el preprocesador lo calcula
		{1, 4, 7},
		{2, 5, 8},
		{3, 6, 9}
	};
	// Decoración
	title ();
	printMatriz (miMatriz);
	printf ("\n\n");
	// Calculos
	double res = 0, subres = 1, res2 = 0;
	// Calculamos las diagonales principales
	system ("toilet -fpagga --gay Principales");
	printf ("\n\n");
	for (int dpx = 0; dpx < DIM; dpx++) {
		subres = 1;
		for (int dpy = 0; dpy < DIM; dpy++) { // dpy es 'Diagonal principal en y'
			printf ("miMatriz[%i][%i] %9.2lf * %9.2lf = ", dpx, dpy, subres, miMatriz[quedateEn3(dpy + dpx)][quedateEn3(dpy)]);
			subres *= miMatriz[quedateEn3(dpy + dpx)][quedateEn3(dpy)];
			printf ("%9.2lf\n", subres);
		}
		printf ("\t%9.2lf + %9.2lf = ", res, subres);
		res += subres;
		printf ("%9.2lf\n", res);
	}
	printf ("Resultado: %9.2lf", res);
	printf ("\n\n");
	system ("toilet -fpagga --gay Secundarias");
	printf ("\n\n");
	res2 = 0;
	for (int dpx = DIM - 1; dpx >= 0; dpx--) {
		subres = 1;
		for (int dpy = DIM - 1; dpy >= 0; dpy--) {
			printf ("miMatriz[%i][%i] %9.2lf * %9.2lf = ", sePositivo(dpx), sePositivo(dpy), subres, miMatriz[sePositivo(dpy - dpx)][sePositivo(dpy)]);
			subres *= miMatriz[sePositivo(dpy - dpx)][sePositivo(dpy)];
			printf ("%9.2lf\n", subres);
		}
		printf ("\t%9.2lf + %9.2lf = ", res2, subres);
		res2 += subres;
		printf ("%9.2lf\n", res2);
	}
	printf ("Resultado: %9.2lf", res2);
	printf ("\n\n");
	printf ("\t\t\t Determinante: %9.2lf.\n\n", res - res2);
    return EXIT_SUCCESS;
}
