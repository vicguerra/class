#include<stdio.h>
#include<string.h>
#define SALTO ('a' - 'A')

int main () {
	char palabra[10];
	int lon;

	printf("Escribe una palabra, sera pasada a mayusculas: ");
	scanf("%s", palabra);

	lon = strlen(palabra);

	printf("%d caracteres\n", lon);
	printf("La palabra en mayusculas es: ");
	for (int i = 0; i < lon; i++) {
		if (palabra[i] > 0x60 && palabra[i] < 0x7B)
			printf("%c", (palabra[i] - SALTO));
		else if (palabra[i] > 0x40 && palabra[i] < 0x5B)
			printf("%c", (palabra[i] + SALTO));
		else
			printf("%c", palabra[i]);
	}
	printf("\n");

	return 0;
}
