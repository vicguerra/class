/*
#################################
##       Diego Martin          ##
#################################
##         Cuadrado            ##
#################################
*/
#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
	int lado;
	printf ("Cuanto quieres que mida su lado?: ");
	scanf ("%d", &lado);
	for (int i = 0; i < lado; i++) {
		for (int j = 0; j < lado; j++)
			printf ("*");
		printf ("\n");
	}
	printf ("\nHecho\n");
	return EXIT_SUCCESS;
}
