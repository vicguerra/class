#include<stdio.h>
#include<stdlib.h>

const char * lista[] = {"Good morning", "Gutten morgen"};

int main (int argc, char *argv[]) {
  const char **p = lista;
  printf ("%p-->%s\n", p, *p);
  p++;
  printf ("%p-->%s\n", p, *p);

  /* Manera 1 */
//  while (*p != NULL) {
//    printf ("%s\n", *p);
//    p++;
//  }

  /* Manera 2 (simple) */
//  while (*(p++) != NULL) printf ("%s\n", *p);

  printf ("\n");
  return EXIT_SUCCESS;
}
