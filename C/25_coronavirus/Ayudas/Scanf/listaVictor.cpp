#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

int main (int argc,char *argv[]) {

    char *texto[]={"hola me llamo Juan", "Hola yo Diego", NULL};
	char *p;
	
	
	// Este los imprime como cadenas
    for (int i = 0; texto[i] != NULL; i++)
        printf("%s\n", texto[i]);
    
    printf ("\n\n");
    // Este los imprime como caracteres
    for (int i=0; texto[i] != NULL; i++) {
    	p = texto[i];
	    while (*p != '\0')
	        printf("%c", *p++);
	    printf ("\n");
	}

	return 0;
}
