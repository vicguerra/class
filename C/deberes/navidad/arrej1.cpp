#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <string.h>

#define R 2
#define G 1
#define B 0
#define COLS 3

const char *colores[] = {
  "Azul",
  "Verde",
  "Rojo"
};

void titulo () {
  system ("clear");
  system ("toilet --gay -fpagga COLORES");
}

void borrar () {
  printf ("                    ");
  printf ("\x1b[10D");
}

unsigned char pedir_color (const char *nombre) {
  int buffer, letra;
  printf ("\n");
  do {
    printf ("\x1b[1A");
    printf ("%s [0-255]: ", nombre);
    borrar ();
    do {
      letra = getchar ();
      if (letra < '0' || letra > '9') {
        __fpurge (stdin);
        printf ("\x1b[1A");
        printf ("%s [0-255]: ", nombre);
        borrar ();
      } else
        ungetc (letra, stdin);
    } while (letra < '0' || letra > '9'); 
    scanf (" %u", &buffer);
    __fpurge (stdin);
  } while (buffer < 0 || buffer > 255);

  return (unsigned char) buffer;
}

void input (unsigned char data[COLS], const char *etiqueta) {
  int longitud = strlen(etiqueta);
  printf ("\n");
  printf ("%s:\n", etiqueta);
  for (int i = 0; i <= longitud; i++)
    printf ("=");
  printf ("\n");
  for (int color = B; color < COLS; color++)
    data[color] = pedir_color (colores[color]);
}

int main (int argc, char *argv[]) {
  unsigned char rgb[COLS], mask[COLS], res[COLS];
  int buffer;
  
  printf ("COLOR\n");
  for (int color = B; color < COLS; color++)
    rgb[color] = pedir_color (colores[color]);
  printf ("MASCARA\n");
  for (int color = B; color < COLS; color++)
    mask[color] = pedir_color (colores[color]);
  printf ("RESULTADO\n");
  for (int i = B; i < COLS; i++)
    res[i] = rgb[i] ^ mask[i];
  printf ("Nuevo color: #%02X%02X%02X xor #%02X%02X%02X = #%02X%02X%02X\n", rgb[R], rgb[G], rgb[B], mask[R], mask[G], mask[B], res[R], res[G], res[B]);
  return EXIT_SUCCESS;
}
