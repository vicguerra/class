#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<typeinfo>
#include<stdarg.h>

//template <class T>
//void print (T text, char end='\n') {
//  int i;
//  // Enteros
//  if (typeid (text) == typeid (int) || typeid (text) == typeid (int *)) // Bien
//    printf ("%i%c", text, end);
//  else if (typeid (text) == typeid (unsigned long int)) // Bien
//    printf ("%lu%c", text, end);
//  else if (typeid (text) == typeid (unsigned int))
//    printf ("%u%c", text, end);
//  else if (typeid (text) == typeid (float)) // Con decimales // Bien (Requiere molde)
//    printf ("%12.12f%c", text, end);
//  else if (typeid (text) == typeid (double) || typeid (text) == typeid (long double))
//    printf ("%d%c", text, end);
//  else if (typeid (text) == typeid (bool)) // Booleanos
//    (text) ? printf ("True%c", end) : printf ("False%c", end); // Bien (Requiere molde)
//  else if (typeid (text) == typeid (char)) // Bien // Caracteres
//    printf ("%c%c", text, end);
//  else if (typeid (text) == typeid (unsigned char)) // Bien (Requiere molde)
//    printf ("%X%c", text, end);
//  else if (typeid (text) == typeid (char *)) // Bien (Requiere molde)
//    printf ("%s%c", text, end);
//    for (i = 0; text[i] != '\0'; i++)
//      printf ("%c", text[i]);
//    printf ("%c\0", end);
//  }
//}

template <class T>
char *formateo (T text) {
  char *parte = "";
  // Enteros
  if (typeid (text) == typeid (int) || typeid (text) == typeid (int *)) // Bien
    parte = ("%i", text);
  else if (typeid (text) == typeid (unsigned long int)) // Bien
    parte = ("%lu", text);
  else if (typeid (text) == typeid (unsigned int))
    parte = ("%u", text);
  else if (typeid (text) == typeid (float)) // Con decimales // Bien (Requiere molde)
    parte = ("%12.12f", text);
  else if (typeid (text) == typeid (double) || typeid (text) == typeid (long double))
    parte = ("%d", text);
  else if (typeid (text) == typeid (bool)) // Booleanos
    parte = (text) ? "True" : "False"; // Bien (Requiere molde)
  else if (typeid (text) == typeid (char)) // Bien // Caracteres
    parte = ("%c", text);
  else if (typeid (text) == typeid (unsigned char)) // Bien (Requiere molde)
    parte = ("%X", text);
  else if (typeid (text) == typeid (char *)) // Bien (Requiere molde)
    parte = ("%s", text);

  return (char *) parte;
}

template <class Tipo>
Tipo cogeTipo () {
  Tipo char;
  
  return Tipo;
}

void print (int num_args, ...) {
  char result[0x1000];
  va_list ap;
  va_start (ap, num_args);
  for (int i = 2; i < num_args; i++) {
    strcat (result, formateo (va_arg (ap, typeid (ap))));
  }
}

int main (int argc, char *argv[]) {
  char *var = "Hola";
  // El BARRACERO!!!
//  print (sizeof(var));
//  print (sizeof("hola"));
//  print ('G');
//  print (45);
//  print (true);
//  print (0x60);
//  print ((unsigned char) 0x60);
//  printf ("%s\n", typeid("Hola").name());
//  print ((double) 23.4);
//  print ((char *)"Hola");
  return EXIT_SUCCESS;
}
