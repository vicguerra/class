/*
#################################
##       Diego Martin          ##
#################################
##         Triangulo           ##
#################################
*/
#include<stdio.h>
#include<stdlib.h>

int main (int argc, char *argv[]) {
	int base;
	printf ("Que base?: ");
	scanf ("%d", &base);
	for (int i = 1; i <= base; i++) {
		for (int j = 1; j<=i; j++) {
			printf ("*");
		}
		printf ("\n");
	}
	return EXIT_SUCCESS;
}

