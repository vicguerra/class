#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
  int base;
  unsigned exp;
  int res = 1;
  
  printf ("Base: ");
  scanf (" %i", &base);
  printf ("Exponente: ");
  scanf (" %u", &exp);

  for (unsigned mul = 0; mul < exp; mul++)
    res *= base;

  if (exp < 0)
    res = 1 / res;

  printf ("Resultado: %i ^ %u = %i.\n", base, exp, res);
  return EXIT_SUCCESS;
}
