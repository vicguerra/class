#include<stdio.h>
#define EURO 166.386

int main () {
	double input;

	printf ("Cuanto quieres cambiar? ");
	scanf (" %lf", &input);
	printf ("%.2lf₧  => %.2lf€\n", input, input / EURO);
	return 0;
}
